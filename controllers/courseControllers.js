const Course = require("../models/Course");

module.exports.addCourse = async (isAdmin, reqBody) => {
	if (isAdmin === true){
		let newCourse = new Course ({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})
		return newCourse.save().then((course, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		})
	} else {
		return "Error: Not an Admin";
	}
}

// Retrieve all Courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	})
}

// Retrieve a specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

// Updating a course
module.exports.updateCourse = (data) => {
	console.log(data)

	return Course.findById(data.courseId).then((result,error) => {
		console.log(result);

		if(data.isAdmin){
			result.name = data.updatedCourse.name
			result.description = data.updatedCourse.description
			result.price = data.updatedCourse.price

			console.log(result);

			return result.save().then((updatedCourse, error) => {
				if(error){
					return false;
				} else {
					return updatedCourse;
				}

			})
		} else {
			return "Not Admin";
		}

	})
}

// Archiving course
module.exports.archiveCourse = (data) => {
	return Course.findById(data.courseId).then((result,error) => {
		if(data.isAdmin){
			result.isActive = false;

			return result.save().then((updatedCourse, error) => {
				if(error){
					return false;
				} else {
					return "Archived";
				}
			})
		} else {
			return "Not Admin";
		}
	})
}